let gulp = require('gulp');
let sass = require('gulp-sass')(require('sass'));
let autoprefixer = require('gulp-autoprefixer');

/**
 * Build sass/scss to css
 */
function buildCss(outputStyle) {
    return gulp.src('Build/scss/main.scss')
        .pipe(sass({
            outputStyle: outputStyle,
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest('Resources/Public/Css/'));
}


gulp.task('build:css', function () {
    return buildCss('expanded');
});

gulp.task('default', gulp.parallel('build:css'));

