# Base development environment

## Get Started
copy all files and run *npm install* in the terminal to install the node_modules or npm packages


This build gulp setup environment is based on this video. It helps to set up the environment, so you can use sass in your project. Here the tutorial for this setup: https://www.youtube.com/watch?v=nI0BfXFjI1I 
For more learnings for sass, please refer to the official page: https://sass-lang.com/guide

Use BEM! http://getbem.com/introduction/, Tutorial: https://www.youtube.com/watch?v=_oP3lHzVZ8I&t=52s
